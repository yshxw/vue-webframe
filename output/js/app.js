webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	var _vue = __webpack_require__(1);

	var _vue2 = _interopRequireDefault(_vue);

	var _vueRouter = __webpack_require__(99);

	var _vueRouter2 = _interopRequireDefault(_vueRouter);

	var _router = __webpack_require__(100);

	var _router2 = _interopRequireDefault(_router);

	var _bootstrapMin = __webpack_require__(105);

	var _bootstrapMin2 = _interopRequireDefault(_bootstrapMin);

	var _fontAwesomeMin = __webpack_require__(114);

	var _fontAwesomeMin2 = _interopRequireDefault(_fontAwesomeMin);

	var _css = __webpack_require__(121);

	var _css2 = _interopRequireDefault(_css);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**

	 * Description App
	 * Power By Mr.K
	 * Copyright www.cnkk.me

	 Please respect the developers, thank you.

	 */

	_vue2.default.use(_vueRouter2.default);

	//引入Css文件


	var router = new _vueRouter2.default();

	//应用路由
	router.map(_router2.default);

	/* 路由监听 */
	router.beforeEach(function (transition) {

	    if (transition.to.path == '/') {
	        transition.redirect('/index');
	    }

	    if (_router2.default[transition.to.path]) {
	        transition.next();
	    } else {
	        transition.redirect('/error');
	    }
	});

	var App = _vue2.default.extend({});
	// start app
	router.start(App, '#app');

/***/ },

/***/ 100:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _index = __webpack_require__(101);

	var _index2 = _interopRequireDefault(_index);

	var _error = __webpack_require__(103);

	var _error2 = _interopRequireDefault(_error);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**

	 * Description Router
	 * Power By Mr.K
	 * Copyright www.cnkk.me

	 Please respect the developers, thank you.

	 */

	exports.default = {

	    '/index': {
	        name: 'index',
	        component: _index2.default
	    },
	    '/error': {
	        name: 'error',
	        component: _error2.default
	    }

	};

/***/ },

/***/ 101:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(Vue) {"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _index = __webpack_require__(102);

	var _index2 = _interopRequireDefault(_index);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Page = Vue.extend({
	    template: _index2.default,
	    components: {
	        alert: alert
	    },
	    data: function data() {
	        //初始化数据
	        return {
	            title: "this is index ~"
	        };
	    },

	    methods: {//事件注册

	    },
	    created: function created() {//数据获取

	        //alert($);

	    }
	}); /**
	    
	     * Description Index
	     * Power By Mr.K
	     * Copyright www.cnkk.me
	    
	     Please respect the developers, thank you.
	    
	     */

	exports.default = Page;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 102:
/***/ function(module, exports) {

	module.exports = "<h1>{{ title }}</h1>\n\n<i class=\"icon-user-md\"></i>";

/***/ },

/***/ 103:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(Vue) {'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _error = __webpack_require__(104);

	var _error2 = _interopRequireDefault(_error);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Page = Vue.extend({
	    template: _error2.default,
	    created: function created() {
	        document.title = '发生错误 ~';
	        this.title = '';
	    }
	}); /**
	    
	     * Description Error
	     * Power By Mr.K
	     * Copyright www.cnkk.me
	    
	     Please respect the developers, thank you.
	    
	     */

	exports.default = Page;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },

/***/ 104:
/***/ function(module, exports) {

	module.exports = "<style type=\"text/css\">\n    .error {\n        color: #666;\n        text-align: center;\n        font-family: Helvetica, 'microsoft yahei', Arial, sans-serif;\n        margin:0;\n        margin: auto;\n        font-size: 14px;\n        margin-top: 30px;\n    }\n    .error h1 {\n        font-size: 120px;\n        line-height: 150px;\n        font-weight: normal;\n        color: #456;\n    }\n    .error h2 { font-size: 40px; color: #666; line-height: 1.5em; }\n\n    .error h3 {\n        color: #456;\n        font-size: 20px;\n        font-weight: normal;\n        line-height: 28px;\n    }\n\n    .error hr {\n        margin: 18px 0;\n        border: 0;\n        border-top: 1px solid #EEE;\n        border-bottom: 1px solid white;\n    }\n\n    .error a{\n        color: #00a9d3;\n        text-decoration: none;\n    }\n</style>\n<div class=\"error\">\n    <h1>404</h1>\n    <h3>你所访问的页面不存在.</h3>\n    <hr>\n    <p>你可能输入了不存在的URL地址，或者该资源已经被删除，或者您无权访问 <a href=\"javascript:void(0);\" v-link=\"{path: '/index'}\">返回首页</a></p>\n    <p>VUE WEB FRAME PowerBy <a href=\"http://www.cnkk.me\" title=\"Mr.K - 专注前端开发\" target=\"_blank\">Mr.K</a> <a href=\"http://www.cnkk.me\" title=\"Mr.K - 专注前端开发\" target=\"_blank\">www.cnkk.me</a></p>\n</div>\n";

/***/ },

/***/ 105:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 114:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 121:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});