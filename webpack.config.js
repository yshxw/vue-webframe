/**

 * Description WebpackConfig
 * Power By Mr.K
 * Copyright www.cnkk.me

 Please respect the developers, thank you.

 */

var path = require("path");
var webpack = require("webpack");

var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");


// webpack.config.js
module.exports = {
    entry: {
        app: "./web/app.js",
        vendor: ["jquery", "bootstrap", "vue", "vue-router"]
    },
    output: {
        path: path.join(__dirname, "output"),
        publicPath: "/vue-webframe/output/",
        filename: "js/[name].js",
        chunkFilename: "lib/[id].bundle.js"
    },
    //加载器
    module: {
        // 加载器
        loaders: [
            // 解析.vue文件
            {
                test: /\.vue$/,
                loader: "vue"
            },
            // 转化ES6的语法
            {
                test: /\.(js|es6)$/,
                loader: "babel",
                exclude: /node_modules/
            },
            // 编译css并自动添加css前缀
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style-loader", "css!autoprefixer")
            },
            //.less 文件想要编译，scss就需要这些东西！来编译处理
            //install css-loader style-loader less-loader --save-dev
            {
                test: /\.less$/,
                loader: "style-loader!css!less"
            },
            // 图片转化，小于8K自动转化为base64的编码
            {
                test: /\.(png|jpg|gif)$/,
                loader: "url-loader",
                query: {
                    limit: 8192,
                    name: "./images/[name].[ext]"
                }
                //在这无论是直接loader 后面跟参数(像url跟参一样)url-loader?limit=8192,或者是后面跟着一个对象 query,都是可以的.类似get请求？
            },
            //字体？
            {
                test: /\.(eot|svg|ttf|otf|woff|woff2)(\?.*)?$/,
                loader: "url-loader",
                query: {
                    limit: 1,
                    name: "/fonts/[name].[ext]"
                }
            },
            //html模板编译？
            {
                test: /\.(html|tpl)$/,
                loader: "html-loader"
            }
        ]
    },
    // .vue的配置。需要单独出来配置
    vue: {
        loaders: {
            css: "style!css!autoprefixer",
            js: "babel"
        }
    },
    // 配置babel转化成es5的语法
    babel: {
        presets: ["es2015"],
        plugins: ["transform-runtime"]
    },
    resolve: {
        // require时省略的扩展名，如：require("module") 不需要module.js
        extensions: ["", ".js", ".es6", ".vue", ".html"],
        // 别名，可以直接使用别名来代表设定的路径以及其他
        alias: {
            jquery: path.join(__dirname, "./web/lib/jquery/jquery.js"),
            vue: path.join(__dirname, "./web/lib/vue/vue.js"),
            bootstrap : path.join(__dirname, "./web/lib/bootstrap/js/bootstrap.js"),
            lib: path.join(__dirname, "./web/lib"),
            common: path.join(__dirname, "./web/common"),
            page: path.join(__dirname, "./web/page")
        }
    },
    plugins: [

        new webpack.ProvidePlugin({ //加载组件
            jQuery: "jquery",
            $: "jquery",
            Vue: "vue",
            "vue-router": "vue-router"
        }),

        //提取公共模块
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            minChunks: Infinity,
            filename: "js/vendor.js",
            warning: false
        }),

        //生成公共Css文件
        new ExtractTextPlugin("css/css.css"),

        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./web/index.html"
        })

    ]
};