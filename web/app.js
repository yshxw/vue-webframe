/**

 * Description App
 * Power By Mr.K
 * Copyright www.cnkk.me

 Please respect the developers, thank you.

 */


import Vue from "vue";
import VueRouter from "vue-router";
import RouterList from "common/router";

//引入Css文件
import bootcss from "lib/bootstrap/css/bootstrap.min.css";
import fontawesome from "lib/fontawesome/css/font-awesome.min.css";
import css from "./css/css.css";

Vue.use(VueRouter);

var router = new VueRouter();

//应用路由
router.map(RouterList);

/* 路由监听 */
router.beforeEach(function(transition){

    if(transition.to.path == '/'){
        transition.redirect('/index');
    }

    if(RouterList[transition.to.path]){
        transition.next();
    }else{
        transition.redirect('/error');
    }

})


var App = Vue.extend({});
// start app
router.start(App, '#app');
