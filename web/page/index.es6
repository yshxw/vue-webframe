/**

 * Description Index
 * Power By Mr.K
 * Copyright www.cnkk.me

 Please respect the developers, thank you.

 */

import html from "page/html/index";

let Page = Vue.extend({
    template: html,
    components: {
        alert
    },
    data () { //初始化数据
        return {
            title: "this is index ~"
        }
    },
    methods: { //事件注册

    },
    created () { //数据获取

        //alert($);

    }

});

export default Page;
