/**

 * Description Error
 * Power By Mr.K
 * Copyright www.cnkk.me

 Please respect the developers, thank you.

 */


import html from "page/html/error"

var Page = Vue.extend({
    template: html,
    created () {
        document.title = '发生错误 ~';
        this.title = '';
    }
});

export default Page;
