/**

 * Description Router
 * Power By Mr.K
 * Copyright www.cnkk.me

 Please respect the developers, thank you.

 */


import p_index from 'page/index';
import p_error from 'page/error';

export default {

    '/index': {
        name: 'index',
        component: p_index
    },
    '/error': {
        name: 'error',
        component: p_error
    }

};
